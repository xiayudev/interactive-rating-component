# Frontend Mentor - Interactive rating component solution

This is a solution to the [Interactive rating component challenge on Frontend Mentor](https://www.frontendmentor.io/challenges/interactive-rating-component-koxpeBUmI). Frontend Mentor challenges help you improve your coding skills by building realistic projects.

## Table of contents

- [Frontend Mentor - Interactive rating component solution](#frontend-mentor---interactive-rating-component-solution)
  - [Table of contents](#table-of-contents)
  - [Overview](#overview)
    - [The challenge](#the-challenge)
    - [Screenshot](#screenshot)
    - [Links](#links)
  - [My process](#my-process)
    - [Built with](#built-with)
    - [What I learned](#what-i-learned)
    - [Continued development](#continued-development)
    - [Useful resources](#useful-resources)
  - [Author](#author)
  - [Acknowledgments](#acknowledgments)

## Overview

### The challenge

Users should be able to:

-   View the optimal layout for the app depending on their device's screen size
-   See hover states for all interactive elements on the page
-   Select and submit a number rating
-   See the "Thank you" card state after submitting a rating

### Screenshot

-   Mobile Design:  
    ![Mobile Design](/public/images/mobile-design.png)
-   Desktop Design:
    ![Desktop Design](public/images/desktop-design.png)

### Links

-   Solution URL: [Gitlab](https://gitlab.com/xiayudev/interactive-rating-component)
-   Live Site URL: [Netlify](https://interactive-ratingcomponent-fm.netlify.app/)

## My process

### Built with

-   Semantic HTML5 markup
-   CSS custom properties
-   Flexbox
-   CSS Grid
-   Mobile-first workflow
-   [Vite](https://vite.dev/)
-   [Tailwind CSS](https://nextjs.org/) - CSS framework

### What I learned

I have learned how to keep :active pseudo-class after clicking an element.

Below, the script works when we use `querySelectorAll()`. This metod returns a NodeList, so we need to acces every object through a for loop.

```js
function activeState(object) {
	for (const key of object) {
		key.addEventListener("click", () => {
			key.classList.toggle("active");
		});
	}
}
```

### Continued development

I am just getting started with JavaScript, with DOM. DOM is really interesting and I want to know how it works behind the scenes. I think we need a deep understanding of it.

### Useful resources

-   [StackOverflow](https://stackoverflow.com/questions/50643302/addeventlistener-on-a-queryselectorall-with-classlist) - This helped me how to keep :active pseudo-class after clicking an element.

## Author

-   Website - [xiayudev](https://my-portfolio-v2-1.pages.dev/)
-   Frontend Mentor - [@TheSunLand7](https://www.frontendmentor.io/profile/TheSunLand7)
-   Twitter - [@J7Jeo](https://www.twitter.com/J7Jeo)

## Acknowledgments

When using `querySelectorAll`, use a for loop to iterate over NodeList.
