const btn = document.getElementById("btn-cta-primary") as HTMLButtonElement;
const card = document.querySelector(".thanks-card") as HTMLDivElement;
const rateNumber = document.querySelectorAll(".rate-circle") as NodeListOf<HTMLDivElement>;
const rateSelected = document.getElementById("rate-selected") as HTMLSpanElement;

export function selectRateNumber(node: NodeListOf<HTMLDivElement>) {
	node.forEach((item) => {
		item.addEventListener("click", () => {
			node.forEach((btn) => {
				btn.classList.remove("active");
			});
			item.classList.add("active");
			rateSelected.textContent = `You selected ${item.textContent} out of 5`;
		});
	});
}

btn.addEventListener("click", () => {
	card.classList.remove("hidden");
});

window.onload = () => {
	selectRateNumber(rateNumber);
};
